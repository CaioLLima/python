from flask import Blueprint, render_template, request, redirect, url_for, flash, current_app, session
from wtforms import Form, StringField, PasswordField
from wtforms.validators import DataRequired
from flask_wtf import FlaskForm
from functools import wraps

bp_login = Blueprint('bp_login', __name__,  template_folder='templates')

class LoginForm(FlaskForm):
    login = StringField(
        'login',
        validators= [
            DataRequired(message="Campo Obrigatório")
        ],
        render_kw= {'placeholder':'Login'}
    )
    password = PasswordField(
        'password',
        validators= [
            DataRequired(message="Campo Obrigatório")
        ],
        render_kw= {'placeholder':'password'}
    )

@bp_login.route("/", methods=['GET', 'POST'])
def formLogin():
    form= LoginForm(request.form)
    if form.validate_on_submit():
        if (request.form.get('login') == 'abc' and request.form.get('password') == 'bolinhas'):
            session['user'] = request.form.get('login')
             #limpa a sessão
            session.clear()
            #registra user na sessão, armazenando o login do usuário
            session['user'] = __name__
            #abre a aplicação na tela home
            return redirect( url_for('home.home'))
        else:
            flash('Usuário invalido, Tente novamente')
    return render_template("formLogin.html",form=form), 200

@bp_login.route("/logoff")
def logoff():
    session.pop('user',None)
    session.clear ()
    return redirect( url_for('bp_login.formLogin'))

    def validaSessao(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            if 'user' not in session:
            #descarta os dados copiados da função original e retorna a tela de login
                return redirect(url_for('login.login',falhaSessao=1))
            else:
            #retorna os dados copiados da função original
                return f(*args, **kwargs)
    #retorna o resultado do if acima
    return decorated_function