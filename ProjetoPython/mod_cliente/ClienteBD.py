from BancoBD import Banco

class Usuarios(object):

    def __init__(self, id_cliente=0, nome="",endereco="",numero="",observacao="",cep="",bairro="",cidade="",estado="",telefone="",email="",login="",senha="",grupo=""):
        self.info = {}
        self.id_cliente = id_cliente
        self.nome = nome
        self.login = login
        self.senha = senha
        self.grupo = grupo
        self.endereco = endereco   
        self.numero = numero 
        self.observacao = observacao   
        self.cep = cep   
        self.bairro = bairro    
        self.cidade = cidade   
        self.estado = estado   
        self.telefone = telefone    
        self.email = email    

    def selectUserALL(self):
        banco=Banco()
        try:
            c=banco.conexao.cursor()
            c.execute("select id_cliente, nome, login, senha, grupo from tb_cliente")
            result = c.fetchall()
            c.close()
            return result
        except:
            return "Ocorreu um erro na busca do usuário"


    def selectUser(self, id_cliente):
        banco=Banco()
        try:
            c=banco.conexao.cursor()
            c.execute("select id_cliente, nome, login, senha, grupo from tb_clientes where id_cliente = %s" , (id_cliente))
            
            for linha in c:
                self.id_cliente=linha[0]
                self.nome=linha[1]
                self.login=linha[2]
                self.senha=linha[3]
                self.grupo=linha[4]
            c.close()

            return "Busca feita com sucesso!"
        except:
            return "Ocorreu um erro na busca do usuário"


    def insertUser(self):

        banco = Banco()
        try:
            c = banco.conexao.cursor()
            c.execute("insert into tb_clientes(nome, login, senha, grupo) values (%s, %s, %s, %s)" , (self.nome, self.login, self.senha, self.grupo ))
            banco.conexao.commit()
            c.close()

            return "Usuário cadastrado com sucesso!"
        except:
            return "Ocorreu um erro na inserção do usuário"


    def updateUser(self):

        banco=Banco()
        try:

            c=banco.conexao.cursor()
            c.execute("update tb_clientes set nome = %s , login  = %s , senha = %s, grupo = %s where id_cliente = %s" , (self.nome , self.login , self.senha, self.grupo, self.id_cliente))
            banco.conexao.commit()
            c.close()

            return "Usuário atualizado com sucesso!"
        except:
            return "Ocorreu um erro na alteração do usuário"


    def deleteUser(self):

        banco=Banco()
        try:

            c=banco.conexao.cursor()
            c.execute("delete from tb_clientes where id_cliente = %s" , (self.id_cliente))
            banco.conexao.commit()
            c.close()

            return "Usuário excluído com sucesso!"
        except:
            return "Ocorreu um erro na exclusão do usuário"