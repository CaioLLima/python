#coding: utf-8
from flask import Flask, session
from flask_bootstrap import Bootstrap
from mod_home.home import bp_home
from mod_cliente.cliente import bp_cliente
from mod_produto.produto import bp_produto
from mod_pedido.pedido import bp_pedido
from mod_login.login import bp_login
import os
import pymysql
from ClienteBD import Clientes

app = Flask(__name__)
bootstrap = Bootstrap(app)

app.secret_key=os.urandom(12).hex()
app.register_blueprint(bp_home)
app.register_blueprint(bp_cliente)
app.register_blueprint(bp_produto)
app.register_blueprint(bp_pedido)
app.register_blueprint(bp_login)

@app.route('/')
def listaUsuarios():
    
    user=Usuarios()
    
    res = user.selectUserALL()
    
    return render_template('formListaClientes.html', result=res, content_type='application/json')


@app.route('/addUser', methods=['GET'])
def addUser():
    
    user=Usuarios()
    
    #user.id_usuario = request.form['id_usuario']
    user.nome  = request.form['nome']
    user.login = request.form['login']
    user.senha = request.form['senha']
    user.grupo = request.form['grupo']

    executou = user.insertUser()
    print(executou)

    return redirect('/')


@app.route('/formEditCliente', methods=['GET'])
def formEditUser():

    user=Usuarios()
    
    #realiza a busca pelo usuario e armazena o resultado no objeto
    executou = user.selectUser( request.form['id_usuario'] )
    print(executou)
    
    return render_template('formCliente.html', user=user, content_type='application/json')


@app.route('/editUser', methods=['GET'])
def editUser():
    
    user=Usuarios()
    
    user.id_usuario = request.form['id_usuario']
    user.nome  = request.form['nome']
    user.login = request.form['login']
    user.senha = request.form['senha']
    user.grupo = request.form['grupo']

    if 'salvaEditaClienteDB' in request.form:
        user.updateUser()
    elif 'excluiClienteDB' in request.form:
        user.deleteUser()

    return redirect('/')

if __name__ == '__main__':
    app.run()